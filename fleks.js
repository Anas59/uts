import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native';

class FlexBox extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style = {styles.flex1}>
                    <Image source={require('/Users/User/nasrul/src/assets/1.jpg')} style={styles.atas}/>
                </View>
                <View style = {styles.flex2}>
                    <ScrollView> 
                    <Image source={require('/Users/User/nasrul/src/assets/2.jpg')} style={styles.cerita}/>
                    <Image source={require('/Users/User/nasrul/src/assets/4.jpg')} style={styles.foto}/>
                    <Image source={require('/Users/User/nasrul/src/assets/5.jpg')} style={styles.foto}/>
                    <Image source={require('/Users/User/nasrul/src/assets/6.jpg')} style={styles.foto}/>
                    <Image source={require('/Users/User/nasrul/src/assets/7.jpg')} style={styles.foto}/>
                    <Image source={require('/Users/User/nasrul/src/assets/8.jpg')} style={styles.foto}/>
                    </ScrollView>    
                </View>
                <View style = {styles.flex3}>
                <Image source={require('/Users/User/nasrul/src/assets/3.jpg')} style={styles.ig}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: 'black'
    },
    flex1: {
        flex: 1, 
        backgroundColor: 'black'
    },
    flex2: {
        flex: 9, 
        backgroundColor: 'black'
    },
    flex3: {
        flex: 1, 
        backgroundColor: 'black'
    },
    atas: {
        height: 65, 
        width: 410
    },
    cerita: {
        height: 110,
        width: 410
    },
    foto: {
        height: 580, 
        width: 430
    },
    ig: {
        height: 52, 
        width: 430
    },
});

export default FlexBox;